package Game;

import java.io.*;
import java.util.ArrayDeque;
import java.util.Deque;

public class GameTable {

    private static final int MAX_TURNS = 20;
    private static final int MAX_LENGTH = 5;

    private String allowedChars = "abcdefghijklmnop1234567";
    private int stringRange=allowedChars.length(); //really necessary?

    private boolean playing=true;
    private boolean debug =false;
    private boolean hard=false;
    private Deque<String> printingList=new ArrayDeque<>();

    /**
     * The Start method is the place where the most important part of the code are expressed.
     * The player is introduced to the game and makes the choices-AI for example- which intialize the game. A 'While' loop
     * controls the game itself, alternating guessing between the computer and the player. The outputs of the code
     * are stored in a list, for potential printing after the game.
     */

    private void start() {

        Player player = new Player();
        printMessage();
        configureGame();
        Computer computer =checkAI();
        player.loadFile();


        String codez= computer.makeComputerCode(MAX_LENGTH,stringRange,allowedChars);
        computer.setCode(codez);

        if (debug) {
            System.out.println("Computer's code is: "+computer.code);
        }

        if(hard){
            computer.printAllKLength(allowedChars,MAX_LENGTH);
        }


        player.makeCode(MAX_LENGTH,allowedChars);
//

        int turncounter=0;


        while(playing) {


            String playerStorage= player.makeGuess(MAX_LENGTH,allowedChars);
            printingList.addLast(playerStorage);
            String playerStorage2=compareTwo(playerStorage, player, computer);
            printingList.addLast(playerStorage2);

            if (player.winner) {
                player.printWin();
                playing=false;
                break;
            }


            String computerStorage = computer.makeGuess(MAX_LENGTH,stringRange,allowedChars);
            printingList.addLast(computerStorage);
            String computerStorage2=compareTwo(computerStorage, computer, player);
            printingList.addLast(computerStorage2);
            turncounter++;
            computer.checkList();
            if (computer.winner) {
                computer.printWin();
                playing=false;
                break;
            }


            if (turncounter==MAX_TURNS) {
                System.out.println("\nNeither player wins! What a shame!");
                playing=false;
                break;
            }


            System.out.println("---------------");

        }

        printMessage();
        saveFile(player,computer);


    }

    /**
     * This method prints a message to the player at the start and end of the game.
     */
    public void printMessage() {

        if (playing) {

            System.out.println("\n~~~~~~******* Welcome to the Bulls and Cows game! *******~~~~~~~"+
                    "\n~~In this game you will try to guess the computer's SECRET CODE!~~"+
                    "\n ~~*BEWARE, for the computer will try to guess yours too!*~~\n");
        }
        else {
            System.out.println("\nThanks for playing!\n");
        }
        System.out.println("                    ********************");
    }

    /**
     *This method prompts the player to select an AI, and returns a different Computer object in response.
     */

    public Computer checkAI() {

        Computer toReturn=new Computer();
        System.out.println("\nPlease choose an AI to play against: \n enter 1 for easy \n enter 2 for medium \n enter 3 for hard");
        boolean choosingAI=true;
        while(choosingAI){

            String chooseAI = Keyboard.readInput();


            if(chooseAI.equals("1")||chooseAI.equals("one")||chooseAI.equals("One")){
                System.out.println("Easy??");
                toReturn=new Computer();
                choosingAI=false;
            }
            else if (chooseAI.equals("2")||chooseAI.equals("two")||chooseAI.equals("Twe")){
                System.out.println("Medium. Fine");
                toReturn=new Medium();
                choosingAI=false;
            }
            else if (chooseAI.equals("3")||chooseAI.equals("three")||chooseAI.equals("Three")){
                System.out.println("Hard. Wow.");
                toReturn=new Hard();
                hard=true;
                choosingAI=false;
            }
            else {
                System.out.println("Please enter an appropriate number:");
            }
        }


        System.out.println("                    ********************");
        return toReturn;
    }

    /**
     * This method tells the player how to configure the game, and allows for 'cheat mode' to be selected.
     */
    public void configureGame() {

        System.out.println("\nTo configure the game, please adjust the constant values in the GameTable class." +
                "\n----To configure the length of the code change the MAX_Length value." +
                "\n----To configure the number of turns in a game change the MAX_TURNS value."+
                "\n----To configure the valid characters, change the allowedChars String."+
                "\nRemember to make your code and guesses in accordance with any configurations.\n");

        System.out.println("                    ********************");

        System.out.println("\nDo you want to start cheat mode? Y/N");
                    String input=Keyboard.readInput();
                    if (input.equals("Y")||input.equals("y")||input.equals("yes")||input.equals("Yes")){
                        debug=true;
                    }else{
                        System.out.println("Very wise.");
                    }

        System.out.println("                    ********************");


    }


    /**
     * This method compares the guess of a Contender with the other's code. The guess is checked against the code, and
     * the number of cows and bulls returned. This code can be used no matter which Contender is doing the guessing.
     */

    public String compareTwo(String guess, Contender guesser, Contender defender) {
        int numBulls = 0;
        int numCows = 0;
        String code = defender.getCode();


        for (int i = 0; i <= MAX_LENGTH - 1; i++) {

            char guessChar= guess.charAt(i);
            char codeChar=code.charAt(i);

            if (guessChar==codeChar) {

                numBulls++;

            } else {

                for (int j = 0; j < MAX_LENGTH; j++) {

                    char jcode=code.charAt(j);
                    if (guessChar == jcode) {
                        numCows++;
                    }

                }

            }

        }


        if (numBulls == MAX_LENGTH) {

            guesser.setWinner();
        }

        if (guesser instanceof Computer){
            guesser.setBullResult(numBulls);
            guesser.setCowResult(numCows);
            guesser.setGuess(guess);
        }
        System.out.println("Result: " + numBulls + " bull(s) and " + numCows + " cow(s)");
        System.out.print("\n");

        return "" + numBulls + " bull(s) and " + numCows + " cow(s)";
    }

    /**
     * This method writes the results of the game to a file using a previously existing Deque. The player can specify a file
     * to be written to.
     */

    public void saveFile(Contender player,Contender computer) {

        boolean choosing = true;

        while (choosing) {
            System.out.println("Do you want to save your game? Y/N");
            String input = Keyboard.readInput();
            if (input.equals("Y") || input.equals("y") || input.equals("yes") || input.equals("Yes")) {
                System.out.println("Please enter a filename: ");
                String writeFile = Keyboard.readInput();

                File myFile =new File(writeFile);
                try(BufferedWriter bw=new BufferedWriter(new FileWriter(myFile))){

                    String playerCode=player.getCode();
                    String computerCode=computer.getCode();


                    bw.write("Bulls and Cows game result\n");
                    bw.write("------------------------\n");
                    bw.write("Your Code: "+playerCode+"\n");
                    bw.write("Computer's code: "+(computerCode)+"\n");
                    bw.write("------------------------\n");



                    for (int i = 0; i <= MAX_TURNS-1; i++) {


                        String myGuess = printingList.removeFirst();
                        System.out.println(myGuess);
                        String myResult = printingList.removeFirst();
                        System.out.println(myResult);
                        String computerGuess = printingList.removeFirst();
                        System.out.println(computerGuess);
                        String computerResult = printingList.removeFirst();
                        System.out.println(computerResult);


                        bw.write("Turn "+(i+1)+":\n");
                        bw.write("You guessed "+myGuess+",scoring "+myResult+"\n");
                        bw.write("Computer guessed "+computerGuess+" scoring "+computerResult+"\n");
                        bw.write("-----------------------\n");

                    }
                    if (player.winner){
                        bw.write("You won!");

                    }
                    else if(computer.winner) {
                        bw.write("The Computer won!");
                    }
                    else{
                        bw.write("The game was a draw!");
                    }
                    System.out.println("File written correctly!");
                    break;

                }catch (IOException e){
                    System.out.println("Error!");
                    System.out.println(e.getMessage());
                }





            }else{
                break;
            }


        }

    }

    public static void main(String[] args) {
        new GameTable().start();
    }


}
