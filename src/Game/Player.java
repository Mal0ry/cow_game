package Game;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Player extends Contender{

   private boolean fileguess=false;
    private String filename;
    private ArrayList<String> guessStorage=new ArrayList<String>();
    public Player() {

    }



    public void makeCode(int length,String chars ) {

        System.out.println("Please enter your secret Code: ");
        String input=generateInput(chars,length);


//        Integer[] toStore=generateInput(input,length);
        this.setCode(input);


    }


    public String generateInput(String chars,int length) {

        String input="";
        boolean checking=true;

        while(checking){
            int result=0;
            String presumtiveinput = Keyboard.readInput();

            for (int i = 0; i <= presumtiveinput.length()-1 ; i++) {

                char toCheck = presumtiveinput.charAt(i);
                result = chars.indexOf(toCheck);
                if (result==-1){
                    System.out.println("there was an Invalid Character in your input. Please try again:");
                    break;
            }

            }
           if ((presumtiveinput.length())<length||(presumtiveinput.length()>length)){
                System.out.println("Please check the length of your input and try again: ");

            }
            else if (checkInput(presumtiveinput)){

                System.out.println("There was a duplicate in your answer. Please try again");


            }else{
               input=presumtiveinput;
               break;

           }

//            checking=false;
            }




        return input;
        }



    public String makeGuess(int length,String chars) {

        String input="1234";



        if(fileguess){

            String changeToInput =guessStorage.get(0);
            input=changeToInput;
            System.out.println("Your guess is: "+changeToInput);
            guessStorage.remove(changeToInput);

        }


       else {

            System.out.println("please enter a guess: ");
            input=generateInput(chars,length);

             System.out.println("You guess " + input);
        }

//        Integer[] toReturn = generateInput(input, length);


        return input;
    }

    public void readFile(){

        File myFile= new File(filename);

        try(Scanner scanner=new Scanner(myFile)) {

            while (scanner.hasNextLine()){
                String fromFile = scanner.nextLine();
                System.out.println(fromFile);
                guessStorage.add(fromFile);
            }



        }catch (FileNotFoundException e){
            System.out.println("Error!");
        }
    }

    public String loadFile() {

        String filename = "";
        boolean choosing = true;
        while (choosing) {
            System.out.println("\nDo you want to load your guesses from a file? Please press Y for 'yes' or N for 'no'.");
            String decision = Keyboard.readInput();
            if (decision.equals("Y")||decision.equals("y")) {
                System.out.println("please enter a filename");
                this.filename=Keyboard.readInput();
                fileguess=true;
                readFile();
                break;
            } else if (decision.equals("N")||decision.equals("n")) {
                System.out.println("okay then");
                break;
            } else {
                System.out.println("Please enter either Yes or No");
            }
        }

        return filename;
    }




    @Override
    public void printWin() {

        System.out.println("You won!");

    }


    public boolean checkInput(String input) {

        boolean badguess=false;
        for (int i = 0; i <=input.length()-1 ; i++) {

            char tocheck=input.charAt(i);
            int counter=0;

            for (int j = 0; j <=input.length()-1 ; j++) {

                char tocheckagainst=input.charAt(j);
                if (tocheck==tocheckagainst){
                    counter++;

                    if (counter>1){
                        badguess=true;
                        break;
                }

                }
            }

        }

        return badguess;
    }
}


